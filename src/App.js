import React from 'react';
import { Route, Routes } from 'react-router-dom';
import IndexSensorHook from './IndexSensorHook';
import Layout from './Layout';
import ApexChartTemp from './ApexChartTemp';



function App() {
  return (
    <Routes>
   
     
        <Route path="/" element={< Layout />}>
      
        <Route path='/' element={<IndexSensorHook />} />
        {/* <Route path='/' element={<ApexChartTemp />} /> */}
        
        </Route>

     
    </Routes>
  );
}

export default App;
