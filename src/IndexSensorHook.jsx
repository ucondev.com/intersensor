import { Client } from "paho-mqtt";
import React, { useEffect, useState } from "react";
import ApexChartPhoto from "./ApexChartPhoto";
import ApexChartPhotoFuzzy from "./ApexChartPhotoFuzzy";
import ApexChartRain from "./ApexChartRain";
import ApexChartSoi from "./ApexChartSoi";
import ApexChartSoiPump from "./ApexChartSoiPump";
import ApexChartSoiVal from "./ApexChartSoiVal";
import ApexChartTemp from "./ApexChartTemp";
import ApexChartTempFuzzy from "./ApexChartTempFuzzy";
import ApexChartWind from "./ApexChartWind";
import ApexChartWindow from "./ApexChartWindow";
export default function IndexSensorHook() {
  const [temperature, setTemperature] = useState({});
  const [dataTemp, setDataTemp] = useState([]);
  const [dataTempFuzzy, setDataTempFuzzy] = useState([]);

  const [photoresistor, setPhotoresistor] = useState({});
  const [dataPhoto, setDataPhoto] = useState([]);
  const [dataPhotoFuzzy, setDataPhotoFuzzy] = useState([]);

  const [SoiValue, setSoiValue] = useState({});
  const [SoiData, setSoiData] = useState([]);
  const [SoiDataFuzzy, setSoiDataFuzzy] = useState([]);
  const [SoiDataValve, setSoiDataValve] = useState([]);
  const [SoiDataPump, setSoiDataPump] = useState([]);

  const [RainValue, setRainValue] = useState({});
  const [RainData, setRainData] = useState([]);

  const [WindValue, setWindValue] = useState({});
  const [WindData, setWindData] = useState([]);

  const [WindowValue, setWindowValue] = useState({});
  const [WindowData, setWindowData] = useState([]);

  const [wind_values, setValue] = useState(0);

  // MQTT broker details
  const brokerUrl = "ws://192.168.1.51:9001/"; // Note the "ws://" for WebSocket connection
  const topics = [
    "results/temperature",
    "results/photoresistor",
    "results/soil",
    "results/wind",
    "results/rain",
    "results/window-control",
  ];

  //  "sensers/wind-sensor";
  const username = "inter00700";
  const password = "Inter00700";

  // Create a new MQTT client
  const client = new Client(brokerUrl, "clientId-" + new Date().getTime());

  // Set up the client's username and password

  const funInputWind = (e) => {
    if (e.key === "Enter") {
      client.connect({
        onSuccess: () => {
          console.log("Connected to MQTT broker");
          // Subscribe to the specified topics

          //assuming messages comes in every 3 seconds to our server and we need to publish or process these messages
          // setInterval(() => {
            console.log('====================================');
            console.log(wind_values);
            console.log('====================================');
            client.send("sensers/wind-sensor", wind_values);
          // }, 3000);
        },
        userName: username,
        password: password,
      });
    }
  };

  useEffect(() => {
    client.connect({
      onSuccess: () => {
        console.log("Connected to MQTT broker");
        // Subscribe to the specified topics
        topics.forEach((topic) => {
          client.subscribe(topic);
        });
      },
      userName: username,
      password: password,
    });
    // Handle incoming messages
    client.onMessageArrived = (message) => {
      // console.log(
      //   `Received message on topic ${message.destinationName}: ${message.payloadString}`
      // );
      // You can handle the incoming messages here.
      if (message.destinationName === "results/temperature") {
        let tempValue = JSON.parse(message.payloadString);

        setTemperature(tempValue);

        setDataTemp((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(tempValue.temperature)],
        ]);

        setDataTempFuzzy((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(tempValue.fan_speed)],
        ]);
      } else if (message.destinationName === "results/photoresistor") {
        let Value = JSON.parse(message.payloadString);
        setPhotoresistor(Value);
        setDataPhoto((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.photoresitor)],
        ]);

        setDataPhotoFuzzy((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.LED_bright)],
        ]);
      } else if (message.destinationName === "results/rain") {
        let Value = JSON.parse(message.payloadString);

        setRainValue(Value);
        setRainData((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.rain)],
        ]);
      } else if (message.destinationName === "results/soil") {
        let Value = JSON.parse(message.payloadString);

        setSoiValue(Value);
        setSoiData((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.soil_value)],
        ]);

        setSoiDataFuzzy((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.pump)],
        ]);

        setSoiDataValve((prevData) => [
          ...prevData,
          [new Date().getTime(), parseInt(Value.valve)],
        ]);

        setSoiDataPump((prevData) => [
          ...prevData,
          [new Date().getTime(), parseInt(Value.pump)],
        ]);
      } else if (message.destinationName === "results/window-control") {
        let Value = JSON.parse(message.payloadString);

        setWindowValue(Value);
        setWindowData((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.window)],
        ]);
      } else if (message.destinationName === "results/wind") {
        let Value = JSON.parse(message.payloadString);
        setWindValue(Value);
        setWindData((prevData) => [
          ...prevData,
          [new Date().getTime(), parseFloat(Value.wind_value)],
        ]);
      }
    };

    // Cleanup: Disconnect from the MQTT broker when the component unmounts
    return () => {
      client.disconnect();
    };
  }, []);

  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-6">
            <ApexChartTemp data={dataTemp} status={temperature} />
          </div>
          <div className="col-6">
            <ApexChartTempFuzzy data={dataTempFuzzy} status={temperature} />
          </div>
          <div className="col-6">
            <ApexChartPhoto data={dataPhoto} status={photoresistor} />
          </div>
          <div className="col-6">
            <ApexChartPhotoFuzzy data={dataPhotoFuzzy} status={photoresistor} />
          </div>

          <div className="col-4">
            Wind sensor:{" "}
            <input
              type="number"
              onChange={(e) => setValue(e.target.value)}
              onKeyDown={(e) => funInputWind(e)}
              className="border-1 p-1"
              value={wind_values}
              min={0}
              max={100}
            />
            <ApexChartWind data={WindData} status={WindValue} />
          </div>
          <div className="col-4">
            <ApexChartRain data={RainData} status={RainValue} />
          </div>
          <div className="col-4">
            <ApexChartWindow data={WindowData} status={WindowValue} />
          </div>

          <div className="col-4">
            <ApexChartSoi data={SoiData} status={SoiValue} />
          </div>
          <div className="col-4">
            <ApexChartSoiVal data={SoiDataValve} status={SoiValue} />
          </div>
          <div className="col-4">
            <ApexChartSoiPump data={SoiDataPump} status={SoiValue} />
          </div>
        </div>
      </div>
    </div>
  );
}
