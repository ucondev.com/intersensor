
const axiosApi = axios.create({
    baseURL: "https://io.adafruit.com/api/v2/inter00700/feeds/",
});

axiosApi.interceptors.response.use(
    (response) => response,
    (error) => Promise.reject(error)
);



// FUNCTION get api
export async function API_GET(path) {
    var headers = {
        headers: {
            'Content-Type': 'application/json',
            "Authorization": "aio_neTO30qQLXeKIn8hvQgaWvzhTWC9",
     
        }
    }
    return await axiosApi.get(path +"?x-aio-key=aio_neTO30qQLXeKIn8hvQgaWvzhTWC9", headers).then((res) => res.data).catch((err) => {
        console.log('====================================');
        console.log(err.request.responseText);
        console.log('====================================');
    });

}