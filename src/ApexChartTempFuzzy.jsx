import React, { useState } from "react";
import ReactApexChart from "react-apexcharts";

const ApexChartTempFuzzy = ({ status, data }) => {
  const [options] = useState({
    chart: {
      id: "realtime",
      height: 350,
      type: "line",
      animations: {
        enabled: true,
        easing: "linear",
        dynamicAnimation: {
          speed: 100,
        },
      },
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: "smooth",
    },
    title: {},
    markers: {
      size: 0,
    },
    xaxis: {
      type: "datetime",
      range: 60000, // 1 minute range for X-axis
      labels: {
        format: "HH:mm:ss",
      },
    },
    yaxis: {
      max: 100,
      min: 0,
    },
    legend: {
      show: false,
    },
  });

  return (
    <div>
      <div id="chart">
        <h2>
          {" "}
          Fan speed status: {status.fan_speed} {status.fan_speed_status}
        </h2>
        <ReactApexChart
          options={options}
          series={[{ data }]}
          type="line"
          height={350}
        />
      </div>
    </div>
  );
};

export default ApexChartTempFuzzy;
